 

 import React from 'react';
 import { StyleSheet, Text, View, TextInput,ImageBackground,Image,TouchableWithoutFeedback,TouchableOpacity,Keyboard,Alert, SafeAreaView, ScrollView } from 'react-native';
 import { Button,Icon,CheckBox,ListItem,Body} from 'native-base';
 import {responsiveHeight,responsiveWidth, responsiveFontSize} from "react-native-responsive-dimensions";
 import HeaderDefault from '../Scr/Components/header';
 export default class SignUp extends React.Component{
   constructor(props) {
     super(props);
    
     this.state = {
       username: '',
       email:'',
       password: '',
       mob:'',
       border0:'black',
       border : 'black',
       border1 : 'black',
       border2: 'black',


     };
     this.validates = this.validates.bind(this);
   }
   validates = () => { 

    let mail = this.state.email;
    let name = this.state.username;
    let pass = this.state.password;
    let mobile = this.state.mob;
    let regname =/[a-zA-Z]/;
    let regemail = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/ ; 
    let regpass = /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})/;
    let regmob =/[0-9]/;
    if(regname.test(name) === false) 
    { 
    this.setState({border0:'red'})
    this.setState({username:name}) 
    return false; 
    } 
    
    else if(regemail.test(mail) === false) 
    { 
    this.setState({border:'red'})
    this.setState({email:mail}) 
    return false; 
    }
    else if(regpass.test(pass) === false) 
    { 
    this.setState({border1:'red'})
    this.setState({password:pass}) 
    return false; 
    } 
    else if(regmob.test(mobile) === false) 
    { 
    this.setState({border2:'red'})
    this.setState({mob:mobile}) 
    return false; 
    }  
    else { 
        this.setState({border0:"blue"})
        this.setState({username:name})
        this.setState({border:"blue"})
        this.setState({email:mail}) 
        this.setState({border1:"blue"})
        this.setState({password:pass}) 
        this.setState({border2:"blue"})
        this.setState({mob:mobile})
        this.props.navigation.navigate('Login')
    
    } 
} 
   
//    onsignup() {
//      const { username, password } = this.state;
//      if(username.length > 0 && password.length > 0)
//      Alert.alert('Login Succesful');
//      else if(username.length==0 && password.length>0)
//      Alert.alert('Fill the Username fields');
//      else if(username.length>0 && password.length==0)
//      Alert.alert('Fill the Password fields');
//      else 
//      Alert.alert('Fill the requried fields (*)');
//    }
 
   render(){
     return(
        
        <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
        <View style={styles.container}>
        <HeaderDefault name="Signup Page"/>
            <View style={styles.sub_container1}>
               <View style={styles.sub_ctnr_logo}>
                    <Image  source={require("../assets/splash.png")}
                            style={{width:100,height:100}} />
                </View>
            </View> 
            <View style={styles.sub_container2} >
                <View style={styles.container_login} >
                    <View style={styles.SectionStyle}>
                        <TextInput
                            style={{flex:1,borderColor:this.state.border0,borderWidth:2,padding:10}}
                            value={this.state.username}
                            onChangeText={(username) => this.setState({ username })}
                            placeholder="Name"
                            underlineColorAndroid="transparent"
                        />
                        <Icon style={styles.icon} type="Entypo" name="user" />
                    
                    </View>
                    
                    <View style={styles.SectionStyle}>
                        <TextInput
                            style={{flex:1,borderColor:this.state.border,borderWidth:2,padding:10}}
                            value={this.state.email}
                            onChangeText={(email) => this.setState({ email })}
                            placeholder="email"
                            underlineColorAndroid="transparent"
                            
                        />
                        <Icon style={styles.icon} type="Zocial" name="gmail" />
                    </View>
                    <View style={styles.SectionStyle}>
                        <TextInput
                            style={{flex:1,borderColor:this.state.border1,borderWidth:2,padding:10}}
                            value={this.state.password}
                            onChangeText={(password) => this.setState({ password })}
                            placeholder="Password"
                            secureTextEntry={true}
                            underlineColorAndroid="transparent"
                        />
                        <Icon style={styles.icon} type="FontAwesome5" name="key" />
                    
                    </View>
                    <View style={styles.SectionStyle}>
                        <TextInput
                            style={{flex:1,borderColor:this.state.border2,borderWidth:2,padding:10}}
                            value={this.state.mob}
                            onChangeText={(mob) => this.setState({mob })}
                            placeholder="Mobile No"
                            underlineColorAndroid="transparent"
                        />
                        <Icon style={styles.icon} type="FontAwesome5" name="mobile-alt" />
                    
                    </View>
                    
                    <View style={{flex:1}}>
                        <Button rounded
                                style={styles.btn}
                                onPress={this.validates.bind(this)}>
                        <Text style={{fontSize:responsiveFontSize(3),color:"black"}}>Sign Up</Text>
                        </Button>
                    </View>
                </View>
                <View style={{flex:1,alignItems:"center",justifyContent:"center"}}>
                    <Text style={{fontSize:responsiveFontSize(3),marginTop:30}}>-------------------OR-------------------</Text>
                </View>
                <View style={styles.login_logos}>
                <TouchableOpacity>
                    <Icon style={{color:"#0066ff",fontSize:40,margin:10}} type="FontAwesome" name="facebook-square" />
                </TouchableOpacity>  
                <TouchableOpacity>
                    <Icon style={{color:"#ff5c33",fontSize:40,margin:10}} type="FontAwesome" name="google-plus-square" />
                </TouchableOpacity>    
                <TouchableOpacity>    
                    <Icon style={{color:"#00ccff",fontSize:40,margin:10}} type="FontAwesome" name="twitter-square" />
                </TouchableOpacity>
                </View>
                
                <View style={{alignItems:"center",justifyContent:"center",marginBottom:10}}>
                    <TouchableOpacity onpress={this.props.navigation.navigate('Login')}>
                        <View><Text>Already have an account? <Text style={{color:"green"}}>Login</Text></Text></View>
                    </TouchableOpacity>
                </View>
            </View>
        </View>
        </TouchableWithoutFeedback>
        
   
     )
   }
 
 }
 const styles = StyleSheet.create({
   container: {
                flex: 1,
               
                justifyContent:'center',
                backgroundColor:"#4bcbcc"
            },
    sub_container1:{
                    flex:1,
                    alignItems:"center", 
                    backgroundColor:"#84FFFF",  
                    width:responsiveWidth(100),
                    borderBottomRightRadius:120
                },
    sub_ctnr_logo:{
                    flex:1,
                    alignItems:"center",
                    backgroundColor:"#ffffff",
                    margin:50,marginBottom:70,
                    borderRadius:10,height:100

                },
    sub_container2:{
                    flex:2,
                    backgroundColor:"#cccccc",
                    width:responsiveWidth(90),
                    borderRadius:20,
                    margin:5
                },
   container_login:{
                     flex:5,
                     justifyContent:'center',
                     alignItems:'center',
                     marginTop:20
                },
    SectionStyle: {  
                    flexDirection: 'row',
                    backgroundColor: '#fff',
                    margin:10,
                    height: responsiveHeight(6),
                    width:responsiveWidth(70),
                    
                },      
    btn: { 
            height: responsiveHeight(6),
            width:responsiveWidth(50),
            alignItems:'center',
            justifyContent:'center',
            backgroundColor:'#4bcbcc',
            margin:20
       },
    icon:{       
            height:responsiveHeight(6),
            width: responsiveWidth(9),
            marginTop:responsiveHeight(0.5),
            marginRight:responsiveWidth(1),
            color:"#666666",
            paddingLeft:4
        },
    login_logos: {
                    flex:1,
                    flexDirection:"row",
                    alignItems:"center",
                    justifyContent:"center"
                },

 
 });
 