import React, { Component } from 'react';
import { Platform, StyleSheet, View, Text, Image, TouchableOpacity, Alert } from 'react-native';
import { responsiveHeight } from 'react-native-responsive-dimensions';
export default  class SplashScreen  extends Component
{
 
  constructor(props){
 
    super(props);
 
    this.state={
 
      isVisible : true,
 
    }
 
  }
 
  Hide_Splash_Screen=()=>{
 
    this.setState({ 
      isVisible : false 
 
    });
 
  }
 
  componentDidMount(){
 
    var that = this;
 
    setTimeout(function(){
 
      that.Hide_Splash_Screen();
 
    }, 5000);
 
 
  
  }
 
    render()
    {
        let Splash_Screen = (
 
            <View style={styles.SplashScreen_RootView}>
 
                <View style={styles.SplashScreen_ChildView}>
 
                    {/* Put all your components Image and Text here inside Child view which you want to show in Splash Screen. */}
 
                    <Image source={require("../assets/splash.png")}
                    style={{width:100, height: 100, resizeMode: 'contain'}} />
                    
                </View>
                
            
        </View> )
 
        return(
            
              
            <View style = { styles.MainContainer }>
                {
                  (this.state.isVisible === true) ? Splash_Screen : this.props.navigation.navigate('Login')
                }
 
            </View>
            
        );
    }
}
 
const styles = StyleSheet.create(
{
    MainContainer:
    {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
 
    SplashScreen_RootView:
    {
        justifyContent: 'center',
        flex:1,
        margin: 10,
        position: 'absolute',
        width: '100%',
        height: '100%',
        
    },
 
    SplashScreen_ChildView:
    {
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#84FFFF',
        flex:1,
    },
});