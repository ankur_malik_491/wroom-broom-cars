 import React from 'react';
 import { StyleSheet, Text, View, TextInput,ImageBackground,Image,TouchableWithoutFeedback,TouchableOpacity,Keyboard,Alert, SafeAreaView, ScrollView } from 'react-native';
 import { Button,Icon,CheckBox,ListItem,Body} from 'native-base';
 import {responsiveHeight,responsiveWidth, responsiveFontSize} from "react-native-responsive-dimensions";
 import HeaderDefault from '../Scr/Components/header';

 export default class Login extends React.Component{
   constructor(props) {
     super(props);
    
     this.state = {
       username: '',
       password: '',
     };
   }
   
   onLogin=()=>{
     const { username, password } = this.state;
     if(username.length > 0 && password.length > 0){
        Alert.alert('Login Succesful');
        this.props.navigation.navigate('Homescreen');
     }
        
     else if(username.length==0 && password.length>0)
     Alert.alert('Fill the Username fields');
     else if(username.length>0 && password.length==0)
     Alert.alert('Fill the Password fields');
     else 
     Alert.alert('Fill the requried fields (*)');
   }
   onPress = () => {
    this.props.navigation.navigate('Homescreen')
  }
   render(){
     return(
        
        <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
        <View style={styles.container}>
            <HeaderDefault name="Login Page"/>
            <View style={styles.sub_container1}>
               <View style={styles.sub_ctnr_logo}>
                    <Image  source={require("../assets/splash.png")}
                            style={{width:100,height:100}} />
                </View>
            </View> 
            <View style={styles.sub_container2} >
                <View style={styles.container_login} >
                    <View style={styles.SectionStyle}>
                        <TextInput
                            style={{flex:1}}
                            value={this.state.username}
                            onChangeText={(username) => this.setState({ username })}
                            placeholder="Username"
                            underlineColorAndroid="transparent"
                        />
                        <Icon style={styles.icon} type="Entypo" name="user" />
                    
                    </View>
                    
                    <View style={styles.SectionStyle}>
                        <TextInput
                            style={{flex:1}}
                            value={this.state.password}
                            secureTextEntry={true}
                            onChangeText={(password) => this.setState({ password })}
                            placeholder="Password"
                            underlineColorAndroid="transparent"
                        />
                        <Icon style={styles.icon} type="FontAwesome5" name="key" />
                    </View>
                    <View style={{flex:1}}>
                        <Button rounded
                                style={styles.btn}
                                onPress={this.onLogin.bind(this)}>
                        <Text style={{fontSize:responsiveFontSize(3)}}>Login</Text>
                        </Button>
                    </View>
                    
                </View>
                <View style={{flex:1,alignItems:"center",justifyContent:"center"}}>
                    <Text style={{fontSize:responsiveFontSize(3),marginTop:60}}>-------------------OR-------------------</Text>
                </View>
                <View style={styles.login_logos}>
                <TouchableOpacity>
                    <Icon style={{color:"#0066ff",fontSize:50,margin:10}} type="FontAwesome" name="facebook-square" />
                </TouchableOpacity>  
                <TouchableOpacity>
                    <Icon style={{color:"#ff5c33",fontSize:50,margin:10}} type="FontAwesome" name="google-plus-square" />
                </TouchableOpacity>    
                <TouchableOpacity>    
                    <Icon style={{color:"#00ccff",fontSize:50,margin:10}} type="FontAwesome" name="twitter-square" />
                </TouchableOpacity>
                </View>
                <View style={{flex:1,alignItems:"center",justifyContent:"center"}}>
                    <TouchableOpacity onPress={this.onPress}>
                        <View><Text>Don't have an account? <Text style={{color:"green"}}>Create new one</Text></Text></View>
                    </TouchableOpacity>    
                        <View >
                        <TouchableOpacity>
                            <Text style={{color:"green",alignItems:"center",textAlign:"center"}}>Forget Username & Password</Text>
                        </TouchableOpacity>
                    </View>
                    
                </View>
                
            </View>
        </View>
        </TouchableWithoutFeedback>
        
        
   
     )
   }
 
 }
 const styles = StyleSheet.create({
   container: {
                flex: 1,
                justifyContent:'center',
                backgroundColor:'#4bcbcc'
            },
    sub_container1:{
                    flex:1,
                    alignItems:"center", 
                    backgroundColor:"#84FFFF",  
                    width:responsiveWidth(100),
                    borderBottomRightRadius:120
                },
    sub_ctnr_logo:{
                    flex:1,
                    alignItems:"center",
                    backgroundColor:"#ffffff",
                    margin:50,marginBottom:65,
                    borderRadius:10,height:100

                },
    sub_container2:{
                    flex:2,
                    backgroundColor:"#cccccc",
                    width:responsiveWidth(90),
                    borderRadius:20,
                    margin:10
                },
   container_login:{
                     flex:2,
                     justifyContent:'center',
                     alignItems:'center',
                     marginTop:20
                },
    SectionStyle: {  
                    flexDirection: 'row',
                    backgroundColor: '#fff',
                    borderWidth:0.5,
                    borderColor: '#000',
                    height: responsiveHeight(6),
                    width:responsiveWidth(70),
                    borderRadius: 40,
                    margin:10,
                    paddingLeft:responsiveWidth(5)
                },      
    btn: { 
            height: responsiveHeight(6),
            width:responsiveWidth(50),
            alignItems:'center',
            justifyContent:'center',
            backgroundColor:'#4bcbcc',
            margin:20
       },
    icon:{       
            height:responsiveHeight(6),
            width: responsiveWidth(8),
            marginTop:responsiveHeight(1),
            marginRight:responsiveWidth(3),
            color:"#666666"
        },
    login_logos: {
                    flex:1,
                    flexDirection:"row",
                    alignItems:"center",
                    justifyContent:"center"
                },

 
 });
 