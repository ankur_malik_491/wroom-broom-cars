import React from 'react';
import { StyleSheet, Text, View,TouchableOpacity, Platform, Alert} from 'react-native';
import {Container, Header, Left, Body, Right, Icon, Input, Content, Footer, FooterTab, Button} from "native-base";
import {responsiveWidth,responsiveHeight,responsiveFontSize} from "react-native-responsive-dimensions";
export default class HeaderDefault extends React.Component {
    constructor(props){
        super(props);
       
    }
    onPress = () => {
        Alert.alert("hello");
      }
    render(){
        
        return ( 
           
                <Header >
                    <Left>
                        <TouchableOpacity onPress={this.onPress}>
                            <Icon style={{color:"white"}} name="arrow-back" />
                        </TouchableOpacity>
                    </Left>
                    <Body>
                         <Text style={styles.headerText}>{this.props.name}</Text>
                    </Body>
                    <Right>
                        <TouchableOpacity>
                            <Icon style={{color:'white'}} type="Entypo" name="dots-three-vertical"/>
                        </TouchableOpacity>
                    </Right>
                </Header>
                
        );
    }
}



const styles = StyleSheet.create({
    headerText:{
        textAlign:"center",
        color:'white',
        fontSize:responsiveFontSize(2.2)
    },
    
});
