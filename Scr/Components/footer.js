import React from 'react';
import { Footer, FooterTab, Button,Icon} from "native-base";
import {responsiveFontSize} from "react-native-responsive-dimensions";

export class FooterDefault extends React.Component {
    constructor(props){
        super(props);
       
    }
  
    render(){
        
        return ( 
            <Footer>
            <FooterTab>
              <Button active>
                 <Icon name="apps"/>
             </Button>
             <Button>
                  <Icon name="camera"/>
             </Button>
             <Button>
                  <Icon name="navigate"/>
             </Button>
             <Button>
                  <Icon name="person"/>
              </Button>
              <Button>
                   <Icon name="refresh"/>
              </Button>
            </FooterTab>
          </Footer>
        );
    }
}

