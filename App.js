import {createAppContainer} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';
import SignUp  from './Screens/signup';
import SplashScreen  from './Screens/splash_screen';
import HomeScreen from './Screens/Homescreen';
import Login from './Screens/login';

const MainNavigator = createStackNavigator({
    Homescreen: HomeScreen,
    Login:Login,
    SignUp:SignUp,
    Splash:SplashScreen,
  },
     { initialRouteName:'Splash',
      headerMode:null}
  );
  
  export default createAppContainer(MainNavigator);
  
  